<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    exclude-result-prefixes="xs"
    version="2.0">
    <xsl:variable name="concatExternalId"><xsl:value-of select="concat(/interesses/relatienummer, /interesses/interessecode)"></xsl:value-of></xsl:variable>
    <xsl:param name="correlatieSleutel"></xsl:param>
    <xsl:param name="toepasselijkheidsdatumtijd"></xsl:param>
    <xsl:output indent="yes"></xsl:output> 
    <xsl:template match="/">
        <jsonObject>
            <businessId><xsl:value-of select="$concatExternalId"></xsl:value-of></businessId>
            <correlatiesleutel><xsl:value-of select="$correlatieSleutel"/></correlatiesleutel>
            <entiteitType>INTEREST</entiteitType>
            <geldigTotMinstens><xsl:value-of select="$toepasselijkheidsdatumtijd"/></geldigTotMinstens>
            <gegevens>
                <jsonObject>
                    <CB_Account__r><CB_RelationNumber__c><xsl:value-of select="/interesses/relatienummer"></xsl:value-of></CB_RelationNumber__c></CB_Account__r>
                    <CB_ExternalID__c><xsl:value-of select="$concatExternalId"/></CB_ExternalID__c>
                    <CB_Date__c><xsl:value-of select="/interesses/wijzigingsdatum"/></CB_Date__c>
                    <CB_Goal__c><xsl:value-of select="/interesses/doel"></xsl:value-of></CB_Goal__c>
                    <xsl:if test="/interesses/uitdrukkingswijze = 'IMPLICIET'">
                        <CB_Implicit__c>true</CB_Implicit__c>
                    </xsl:if>
                    <xsl:if test="/interesses/uitdrukkingswijze = 'EXPLICIET'">
                        <CB_Implicit__c>false</CB_Implicit__c>
                    </xsl:if>
                    <CB_PrivacyStatement__c><xsl:value-of select="/interesses/privacyindicatiecode"/></CB_PrivacyStatement__c>
                    <CB_Interest_Subject__r><CB_ExternalID__c><xsl:value-of select="/interesses/interessecode"></xsl:value-of></CB_ExternalID__c></CB_Interest_Subject__r>
                    <xsl:if test="/interesses/interesseindicatie = 'ja'">
                        <CB_Positive__c>true</CB_Positive__c>
                    </xsl:if>
                    <xsl:if test="/interesses/interesseindicatie = 'nee'">
                        <CB_Positive__c>false</CB_Positive__c>
                    </xsl:if>
                </jsonObject>
            </gegevens>
        </jsonObject>
    </xsl:template>
</xsl:stylesheet>	