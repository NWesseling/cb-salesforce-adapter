<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:sf="urn:sobject.enterprise.soap.sforce.com" xmlns:ent="urn:enterprise.soap.sforce.com"
    exclude-result-prefixes="#all">
    <xsl:strip-space elements="*"/>
    <xsl:output indent="no"/>
	<xsl:template match="*">
        <xsl:element name="{lower-case(name())}">
            <xsl:apply-templates select="@* | node()"/>
        </xsl:element>
    </xsl:template>
    
    <xsl:template match="ent:queryResponse">         

        <interesses>    
            <xsl:processing-instruction name="xml-multiple"/>
            <xsl:apply-templates select="descendant::ent:records"/>
        </interesses>
    </xsl:template>

    <xsl:template match="ent:records">

        <interesse>
        	<interesseid>
        		<xsl:value-of select="sf:Id"/>
        	</interesseid>
        	<relatienummer>
        		<xsl:value-of select="sf:CB_Account__r/sf:CB_RelationNumber__c"/>
        	</relatienummer>
            <onderwerp>
                <xsl:value-of select="sf:CB_Interest_Subject__r/sf:CB_ExternalID__c"/>
            </onderwerp>
            <privacybepaling>
                <xsl:value-of select="sf:CB_PrivacyStatement__c"/>
            </privacybepaling>
            <interesseindicatie>
                <xsl:choose>
		            <xsl:when test="sf:CB_Positive__c = 'true'">JA</xsl:when>
		            <xsl:otherwise>NEE</xsl:otherwise>
		        </xsl:choose>
            </interesseindicatie>
            <uitdrukkingswijze>
               <xsl:choose>
		            <xsl:when test="sf:CB_Implicit__c = 'true'">IMPLICIET</xsl:when>
		            <xsl:otherwise>EXPLICIET</xsl:otherwise>
		        </xsl:choose>
            </uitdrukkingswijze>
            <doel>
                <xsl:value-of select="sf:CB_Goal__c"/>
            </doel>
        </interesse>

    </xsl:template>
</xsl:stylesheet>
