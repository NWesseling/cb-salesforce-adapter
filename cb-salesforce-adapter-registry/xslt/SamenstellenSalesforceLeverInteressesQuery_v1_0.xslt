<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:urn="urn:enterprise.soap.sforce.com">
    <xsl:param name="relatienummer"/>
    <xsl:template match="/">
        <urn:query>
            <urn:queryString>SELECT ID,
            CB_Goal__c,
            CB_PrivacyStatement__c,
            CB_Positive__c,
            CB_Interest_Subject__r.CB_ExternalID__c,       
            CB_Implicit__c,
            CB_Account__r.CB_RelationNumber__c,
            CB_Date__c
            FROM CB_Interest__c
            WHERE CB_Account__r.CB_RelationNumber__c = '<xsl:value-of select="$relatienummer"/>'</urn:queryString>
        </urn:query>
    </xsl:template>
</xsl:stylesheet>
