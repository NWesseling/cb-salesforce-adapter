<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:ns1="http://nl.consumentenbond/service/subscriptionservice/v1"
    exclude-result-prefixes="#all" xmlns:sub="http://nl.consumentenbond/data/subscription/v1"
    version="2.0" xmlns:general="http://nl.consumentenbond/data/generic/v1" xmlns:prod="http://nl.consumentenbond/data/product/v1">
    <xsl:param name="SAPTimeStamp"></xsl:param>
    <xsl:param name="correlatieSleutel"></xsl:param>
    <xsl:strip-space elements="*"/>
    <xsl:output indent="yes"/>  
    <xsl:template match="/">
         <xsl:apply-templates select="//contractRegelVerwijdering"/>
    </xsl:template>
    
    <xsl:template match="contractRegelVerwijdering">
        
        <xsl:variable name="businessId">
            <xsl:value-of select="concat(contractnummer,'-', positienummer)"/>            
        </xsl:variable>
        
        <jsonObject>
            <businessId><xsl:value-of select="$businessId"></xsl:value-of></businessId>
            <correlatiesleutel><xsl:value-of select="$correlatieSleutel"/></correlatiesleutel>
            <entiteitType>CONTRACT</entiteitType>
            <gegevens>
                <Account> 
                    <CB_RelationNumber__c><xsl:value-of select="relatienummer"/></CB_RelationNumber__c>
                </Account>
                <CB_EndDatePosition__c><xsl:value-of select="verwijderdatum"/></CB_EndDatePosition__c>
                <CB_PositionNumber__c><xsl:value-of select="positienummer"/></CB_PositionNumber__c>
                <Name><xsl:text>Verwijderde_positie</xsl:text></Name>
                <CB_ExternalID__c><xsl:value-of select="$businessId"/></CB_ExternalID__c>
                <Name><xsl:text>Verwijderde_positie</xsl:text></Name>
                <!-- CB_TerminationReason__c><xsl:value-of select="../sub:TerminateReason"></xsl:value-of></CB_TerminationReason__c-->
                <CB_ContractNumber__c><xsl:value-of select="contractnummer"/></CB_ContractNumber__c>
                <CB_EndDateContract__c><xsl:value-of select="verwijderdatum"/></CB_EndDateContract__c>
                <CB_DeletionMarker__c>true</CB_DeletionMarker__c>
            </gegevens>
            <geldigTotMinstens><xsl:value-of select="$SAPTimeStamp"/></geldigTotMinstens>
        </jsonObject>
    </xsl:template>
</xsl:stylesheet>
