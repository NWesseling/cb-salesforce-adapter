<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="2.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:ou="http://soap.sforce.com/2005/09/outbound"
    xmlns:ns0="urn:sobject.enterprise.soap.sforce.com"
    exclude-result-prefixes="#all">
    
    <xsl:strip-space elements="*"/>
    <xsl:output indent="yes"/>
    <xsl:template match="ou:sObject">
        <xsl:processing-instruction name="xml-multiple"/>
        <relatie>
            <relatienummer>
            	<xsl:value-of select="ns0:CB_RelationNumber__c"/>
        	</relatienummer>
        	
        	<emailadres>
                <xsl:value-of select="ns0:CB_Email__c"/>
            </emailadres>
            
        	
        	<voornaam>
	            <xsl:value-of select="ns0:CB_FirstName__c"/>
	        </voornaam>
	        
	        <voorletters>
	            <xsl:value-of select="translate(ns0:CB_Initials__c, ' ', '')"/>
	        </voorletters>
	        
	        <tussenvoegsels>
	        	<xsl:value-of select="ns0:CB_Prefix__c"/>
	        </tussenvoegsels>
	        
	        <achternaam>
	            <xsl:value-of select="ns0:CB_LastName__c"/>
	        </achternaam>
            
            <geboortedatum>
	            <xsl:value-of select="ns0:CB_Birthdate__c"/>
	        </geboortedatum>
	        
	        <overlijdensdatum>
	            <xsl:value-of select="ns0:CB_DayDeceased__c"/>
	        </overlijdensdatum>
	        
	        <geslacht>
	        	<xsl:choose>
	        		<xsl:when test="ns0:CB_Gender__c[1]='Man'">
	        			<xsl:text>MAN</xsl:text>
	        		</xsl:when>
	        		<xsl:when test="ns0:CB_Gender__c[1]='Vrouw'">
	        			<xsl:text>VROUW</xsl:text>
	        		</xsl:when>
	        		<xsl:otherwise>
	        			<xsl:text>ONBEKEND</xsl:text>
	        		</xsl:otherwise>
	        	</xsl:choose>	        
	        </geslacht>
	        
	        
            
            <facturatiegegevens>
	            <IBAN>
	                <xsl:value-of select="ns0:CB_IBAN__c[1]"></xsl:value-of>
	            </IBAN>
	            
	            <!-- Uitgeschakeld omdat afgesproken is dat betaalmethode nog niet meegaat in fase II -->
	            <!-- xsl:if test="ns0:CB_PaymentMethod__c != ''">
	            	<incasso>
	            		<xsl:choose>
	            			<xsl:when test="ns0:CB_PaymentMethod__c = 'Automatisch Incasso'">
	            				<xsl:text>JA</xsl:text>
	            			</xsl:when>
	            			<xsl:when test="ns0:CB_PaymentMethod__c = 'Acceptgiro'">
	            				<xsl:text>NEE</xsl:text>
	            			</xsl:when>
	            		</xsl:choose>
	            	</incasso>
	            </xsl:if -->
	        </facturatiegegevens>
            
            <xsl:processing-instruction name="xml-multiple"/>
            <adressen>
                
                <xsl:choose>
                	<xsl:when test="not(ns0:CB_BillingStreet__c) or ns0:CB_BillingStreet__c[1] = ''">
                		<postbus><xsl:text>JA</xsl:text></postbus>
                		
                		<straatnaam/>
                		
		                <huisnummer>
		                	<xsl:value-of select="ns0:CB_PostbusNumber__c[1]"/>
		                </huisnummer>
		                		                		                
		                <toevoeging/>
		                
		                <postcode>
			                <postcode>
			                	<xsl:value-of select="ns0:CB_PostbusPostalCode__c[1]"/>
			                </postcode>
			                <land>
			                	<xsl:text>NL</xsl:text>
			                </land>
		                </postcode>
		                
		                <plaatsnaam>
		                	<xsl:value-of select="ns0:CB_PostbusCity__c[1]"/>
		                </plaatsnaam>
                    </xsl:when>
                    <xsl:otherwise>
                    	<postbus><xsl:text>NEE</xsl:text></postbus>
                    	
                    	<straatnaam>
		                   <xsl:value-of select="ns0:CB_BillingStreet__c[1]"/>
		                </straatnaam>
		                
		                <huisnummer>
		                  <xsl:value-of select="ns0:CB_BillingHouseNumber__c[1]"/>
		                </huisnummer>
		                
		                
		                <toevoeging>
		                   <xsl:value-of select="ns0:CB_BillingHouseNumberAnnex__c[1]"/>
		                </toevoeging>
		                
		                <postcode>
			                <postcode>
			                	<xsl:value-of select="ns0:CB_PostalCode__c[1]"/>
			                </postcode>
			                <land>NL</land>
		                </postcode>
		                
		                <plaatsnaam>
		                	<xsl:value-of select="ns0:CB_City__c[1]"/>
		                </plaatsnaam>
                    </xsl:otherwise>
                </xsl:choose>
               
            </adressen>
            
            <xsl:processing-instruction name="xml-multiple"/>
            <contactgegevens>
            	<type>TEL_PRIMAIR</type>
            	<telefoon>
            		<xsl:value-of select="ns0:CB_Phone__c[1]"/>
            	</telefoon>
            </contactgegevens>
                     
      
            <xsl:if test="ns0:CB_EmailContactChannel__c = 'true'">
                <xsl:processing-instruction name="xml-multiple"/>
                <contactkanalen>
                 <contactkanaal>EMAIL</contactkanaal>
                 <toestemming>JA</toestemming>
                </contactkanalen>
            </xsl:if>
            <xsl:if test="ns0:CB_EmailContactChannel__c = 'false'">
               <xsl:processing-instruction name="xml-multiple"/>	
               <contactkanalen>
                 <contactkanaal>EMAIL</contactkanaal>
                 <toestemming>NEE</toestemming>
               </contactkanalen>
             </xsl:if>	
           
    
            <herkomst>H01</herkomst>
        </relatie>
    </xsl:template>
    
    
    
    <xsl:template match="ou:Id">
    </xsl:template>
    
    
</xsl:stylesheet>