<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:v1="http://nl.consumentenbond/service/packagesubscription/v1" xmlns:ns0="urn:sobject.enterprise.soap.sforce.com" exclude-result-prefixes="#all">
	<xsl:output indent="yes"/>
	<xsl:template match="/">
		<v1:cancelPackageSubscriptionRequest>
			<v1:RelationId>
				<xsl:value-of select="//ns0:CB_RelationNumber__c[1]"/>
			</v1:RelationId>
			<v1:PackageSubscriptionId>
				<xsl:value-of select="//ns0:CB_ContractNumber__c[1]"/>
			</v1:PackageSubscriptionId>
			<v1:EndDate>
				<xsl:value-of select="//ns0:CB_EndDateContract__c[1]"/>
			</v1:EndDate>
			<v1:TerminationReason>
				<xsl:value-of select="//ns0:CB_TerminationReason__c[1]"/>
			</v1:TerminationReason>
		</v1:cancelPackageSubscriptionRequest>	
	</xsl:template>
</xsl:stylesheet>