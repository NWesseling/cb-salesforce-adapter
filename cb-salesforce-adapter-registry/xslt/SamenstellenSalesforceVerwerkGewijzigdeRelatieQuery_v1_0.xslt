<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    exclude-result-prefixes="xs"
    version="2.0">
    <xsl:output indent="yes"></xsl:output>
    <xsl:param name="correlatieSleutel"/>
    <xsl:param name="deletionIndicatorCheck"/>
    <xsl:template match="/">
        <jsonObject>
            <businessId>
                <xsl:choose>
                    <xsl:when test="$deletionIndicatorCheck = 'D'">
                        <xsl:value-of select="/jsonObject/relatienummer"/>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:value-of select="/jsonObject/relatie/relatienummer"/>                        
                    </xsl:otherwise>
                </xsl:choose>                       
            </businessId>
            <correlatiesleutel><xsl:value-of select="$correlatieSleutel"/></correlatiesleutel>
            <entiteitType>RELATION</entiteitType>
            <gegevens>
                <xsl:apply-templates select="@* | node()" />
            </gegevens>
            <geldigTotMinstens><xsl:value-of select="/jsonObject/berichtverzoek/initiator/toepasselijkheidsdatumtijd"/></geldigTotMinstens>
        </jsonObject>  
    </xsl:template>
    <xsl:template match="/jsonObject/berichtverzoek">
        <xsl:if test="$deletionIndicatorCheck = 'D'">
            <CB_DeletionMarker__c>true</CB_DeletionMarker__c>
        </xsl:if>
         <xsl:if test="$deletionIndicatorCheck != 'D'">
            <CB_DeletionMarker__c>false</CB_DeletionMarker__c>
        </xsl:if>
    </xsl:template>
    
    
    <xsl:template match="/jsonObject/relatienummer">
        <CB_RelationNumber__c><xsl:value-of select="."/></CB_RelationNumber__c>
    </xsl:template>
    <xsl:template match="/jsonObject/wijzigingindicator"/>
    <xsl:template match="/jsonObject/relatie">
        <CB_RelationNumber__c><xsl:value-of select="relatienummer"/></CB_RelationNumber__c>
        <CB_Gender__c><xsl:value-of select="geslacht"/></CB_Gender__c>
        <PersonBirthdate><xsl:value-of select="geboortedatum"/></PersonBirthdate>
        <PersonEmail><xsl:value-of select="emailadres"/></PersonEmail>
        <FirstName><xsl:value-of select="voornaam"/></FirstName>
        <LastName><xsl:value-of select="achternaam"/></LastName>        
        <CB_Prefix__c><xsl:value-of select="tussenvoegsels"/></CB_Prefix__c>
        <MiddleName><xsl:value-of select="tussenvoegsels"/></MiddleName>
        <xsl:choose>
            <xsl:when test="//adressen/postbus = 'NEE'">
                <CB_BillingHouseNumber__c><xsl:value-of select="adressen/huisnummer"/></CB_BillingHouseNumber__c>
                <CB_BillingHouseNumberAnnex__c><xsl:value-of select="adressen/toevoeging"/></CB_BillingHouseNumberAnnex__c>
                <CB_BillingStreet__c><xsl:value-of select="adressen/straatnaam"/></CB_BillingStreet__c>
                <CB_City__c><xsl:value-of select="adressen/plaatsnaam"/></CB_City__c>
                <CB_PostalCode__c><xsl:value-of select="adressen/postcode/postcode"/></CB_PostalCode__c>
            </xsl:when>
            <xsl:otherwise>
                <CB_PostbusCity__c><xsl:value-of select="adressen/plaatsnaam"/></CB_PostbusCity__c>
                <CB_PostbusNumber__c><xsl:value-of select="adressen/huisnummer"/></CB_PostbusNumber__c>  
                <CB_PostbusPostalCode__c><xsl:value-of select="adressen/postcode/postcode"/></CB_PostbusPostalCode__c>
            </xsl:otherwise>
        </xsl:choose> 
        <xsl:if test="contactgegevens[type='TEL_PRIMAIR']/telefoon != ''">
            <Phone><xsl:value-of select="contactgegevens[type='TEL_PRIMAIR']/telefoon"/></Phone>
        </xsl:if>
        <CB_Countries__c><xsl:value-of select="adressen/postcode/land"/></CB_Countries__c>
        <CB_Country__c><xsl:value-of select="adressen/postcode/land"/></CB_Country__c>
        <CB_DayDeceased__c><xsl:value-of select="overlijdensdatum"></xsl:value-of></CB_DayDeceased__c>
        <CB_IBAN__c><xsl:value-of select="facturatiegegevens/IBAN"/></CB_IBAN__c>
        
        <!-- Uitgeschakeld omdat afgesproken is dat betaalmethode nog niet meegaat in fase II -->
        <!-- xsl:if test="facturatiegegevens/incasso != ''">
        	<CB_PaymentMethod__c>
           		<xsl:choose>
           			<xsl:when test="facturatiegegevens/incasso = 'JA'">
           				<xsl:text>Automatisch Incasso</xsl:text>
           			</xsl:when>
           			<xsl:when test="facturatiegegevens/incasso = 'NEE'">
           				<xsl:text>Acceptgiro</xsl:text>
           			</xsl:when>
           		</xsl:choose>
	        </CB_PaymentMethod__c>
        </xsl:if -->
        <CB_Initials__c><xsl:value-of select="voorletters"/></CB_Initials__c>   
   
        <xsl:for-each select="contactkanalen">
            <xsl:choose>
               <xsl:when test="toestemming = 'JA' and contactkanaal = 'POST'"><CB_MailContactChannel__c>true</CB_MailContactChannel__c></xsl:when>
               <xsl:when test="toestemming = 'NEE' and contactkanaal = 'POST'"><CB_MailContactChannel__c>false</CB_MailContactChannel__c></xsl:when>
            </xsl:choose>
            <xsl:choose>
                <xsl:when test="toestemming = 'JA' and contactkanaal = 'EMAIL'"><CB_EmailContactChannel__c>true</CB_EmailContactChannel__c></xsl:when>
                <xsl:when test="toestemming = 'NEE' and contactkanaal = 'EMAIL'"><CB_EmailContactChannel__c>false</CB_EmailContactChannel__c></xsl:when>
            </xsl:choose>
            <xsl:choose>
                <xsl:when test="toestemming = 'JA' and contactkanaal = 'TELEFOON'"><CB_PhoneContactChannel__c>true</CB_PhoneContactChannel__c></xsl:when>
                <xsl:when test="toestemming = 'NEE' and contactkanaal = 'TELEFOON'"><CB_PhoneContactChannel__c>false</CB_PhoneContactChannel__c></xsl:when>
            </xsl:choose>
        </xsl:for-each>
        <CB_PriceGroup__c><xsl:value-of select="prijsgroep"/></CB_PriceGroup__c>
        <CB_RelationStatus__c><xsl:value-of select="status"/></CB_RelationStatus__c>
    </xsl:template>
</xsl:stylesheet>