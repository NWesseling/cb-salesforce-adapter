<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:ns1="http://nl.consumentenbond/service/subscriptionservice/v1"
    exclude-result-prefixes="#all" xmlns:sub="http://nl.consumentenbond/data/subscription/v1"
    version="2.0" xmlns:general="http://nl.consumentenbond/data/generic/v1" xmlns:prod="http://nl.consumentenbond/data/product/v1">
    <xsl:param name="SAPTimeStamp"></xsl:param>
    <xsl:param name="correlatieSleutel"></xsl:param>
    <xsl:strip-space elements="*"/>
    <xsl:output indent="yes"/>  
    <xsl:template match="ns1:Result"/>
    <xsl:template match="ns1:Subscription">
        <alleregels>
            <xsl:apply-templates select="sub:SubscriptionItem"/>
        </alleregels>
    </xsl:template>
    
    <xsl:template match="sub:SubscriptionItem">
        
        <xsl:variable name="businessId">
            <xsl:choose>
                <xsl:when test="sub:SubscriptionItemId != ''">
                    <xsl:value-of select="concat(../sub:SubscriptionId,'-', sub:SubscriptionItemId)"/>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:value-of select="concat(../sub:SubscriptionId,'-', sub:StartDate)"/>
                </xsl:otherwise>
            </xsl:choose>
        </xsl:variable>
        
        <jsonObject>
            <businessId><xsl:value-of select="$businessId"></xsl:value-of></businessId>
            <correlatiesleutel><xsl:value-of select="$correlatieSleutel"/></correlatiesleutel>
            <entiteitType>CONTRACT</entiteitType>
            <gegevens>
                <Account> 
                    <CB_RelationNumber__c><xsl:value-of select="../sub:RelationId"/></CB_RelationNumber__c>
                </Account>
                <CB_StartDatePosition__c><xsl:value-of select="sub:StartDate"/></CB_StartDatePosition__c>
                <CB_EndDatePosition__c><xsl:value-of select="sub:EndDate"/></CB_EndDatePosition__c>
                <Product2>
                    <CB_ProductCode__c><xsl:value-of select="sub:Product/prod:ProductCode"></xsl:value-of></CB_ProductCode__c>
                </Product2>
                <Name><xsl:value-of select="sub:Product/prod:ProductDescription"/></Name>
                <CB_PositionNumber__c><xsl:value-of select="sub:SubscriptionItemId"/></CB_PositionNumber__c>
                <CB_ExternalID__c><xsl:value-of select="$businessId"/></CB_ExternalID__c>
                <CB_TerminationReason__c><xsl:value-of select="../sub:TerminateReason"></xsl:value-of></CB_TerminationReason__c>
                <CB_ContractNumber__c><xsl:value-of select="../sub:SubscriptionId"/></CB_ContractNumber__c>
                <CB_StartDateContract__c><xsl:value-of select="../sub:StartDate"/></CB_StartDateContract__c>
                <CB_EndDateContract__c><xsl:value-of select="../sub:EndDate"/></CB_EndDateContract__c>
                <CB_ActionCode__c><xsl:value-of select="sub:WBS"/></CB_ActionCode__c>
                <CB_CurrentPaymentPeriodEndDate__c><xsl:value-of select="sub:ExpirationDate"></xsl:value-of></CB_CurrentPaymentPeriodEndDate__c>
                <CB_SAPTimeStamp__c><xsl:value-of select="$SAPTimeStamp"/></CB_SAPTimeStamp__c>
            </gegevens>
            <geldigTotMinstens><xsl:value-of select="$SAPTimeStamp"/></geldigTotMinstens>
        </jsonObject>
    </xsl:template>
</xsl:stylesheet>